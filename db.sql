create table task (
    id int identity primary key,
    description varchar(255) not null,
    done boolean default false,
    added timestamp default CURRENT_TIMESTAMP
    )
